#coding: utf-8
from django.conf.urls import url

from loja_app.views.cadastar_carro import Cadastar_Carro
from loja_app.views.serach import Busca
from loja_app.views.index import Index
from loja_app.views.usuario import Cadastro, Login_usuario, Logout_usuario
from loja_app.views.ver_carro import Ver_carro

urlpatterns = [
    url(r'^index/$', Index.as_view(), name='index'),
    url(r'^search/$', Busca.as_view(), name='Search1'),
    url(r'^cadastro/$', Cadastro.as_view(), name='cadastro'),
    url(r'^login$', Login_usuario.as_view(), name='login'),
    url(r'^logout/$', Logout_usuario.as_view(), name='logout'),
    url(r'^cadastrar_carro/$', Cadastar_Carro.as_view(), name='Cadastar_Carro'),
    url(r'^ver_carro/(?P<id>\d+)/$', Ver_carro.as_view(), name='ver_carro')
]