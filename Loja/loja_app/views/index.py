#coding: utf-8
from django.views.generic.base import View
from django.shortcuts import render

from loja_app.models import Carros


class Index(View):

    def get(self, request):

        carros = Carros.objects.all()
        context_dict = {}
        return render(request, 'index.html', {'carros':carros})


