#coding: utf-8
from django.views.generic.base import View
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from loja_app.forms.usuario import UsuarioForm
from django.http import HttpResponseRedirect, HttpResponse
from loja_app.models.usuario import Usuario

class Cadastro(View):


    def post(self, request):
        form = UsuarioForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect('/loja/login')
        else:
            print(form.errors)
            return render(request, 'cadastro.html/', {'form': form})

    def get(self, request):
        form = UsuarioForm()
        return render(request, 'cadastro.html/', {'form': form})





class AtivaCadastro(View):

    template = 'cadastro.html/'
    conteudo = {'form':UsuarioForm()}

    def get (self,request, chaveAtivacao=None):
        self.conteudo.update({'messengerSucess': '', 'messengerError': ''})

        if (chaveAtivacao):
            try:
                usuario = Usuario.objects.get(chaveAtivacaoUsuario=str(chaveAtivacao))
            except:
                usuario = None

            if (usuario):
                if not (usuario.is_active):
                    usuario.is_active = True
                    usuario.save()
                    self.conteudo.update({'messengerSucess': 'Cadastro ativado!'})
                else:
                    self.conteudo.update({'messengerError': 'Chave já foi utilizada!'})

            else:
                self.conteudo.update({'messengerError':'Chave não associada a nenhum Cadastro!'})
        else:
            self.conteudo.update({'messengerError': 'Nenhuma chave informada!'})

        return render(request, self.template, self.conteudo)




class Login_usuario(View):
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('search/')
            else:
                return HttpResponse("Sua conta esta desativada.")
        else:
            print("Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("login invalido.")
    def get(self, request):
        return render(request, 'login.html/', {})

class Logout_usuario(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/loja/index/')