#coding: utf-8
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic.base import View

from loja_app.forms.Form_Cadastar_Carro import Form_Cadastar_Carro


class Cadastar_Carro(View):

    template = "Cadastar_Carro.html"
    conteudo = {'form': Form_Cadastar_Carro()}

    def post(self, request):
        form = Form_Cadastar_Carro(request.POST)
        if form.is_valid():
            form.save(commit=True)
            print ("entrou")
            self.conteudo.update({'messengerSucess': 'Cadastrado com Sucesso', 'messengerError': ''})
            #return HttpResponseRedirect('/naiv_app/cadastar_prova/')
        else:
            print(form.errors)
            return render(request, self.template, {'form': form})
        return render(request, self.template, self.conteudo)

    def get(self, request):
        form = Form_Cadastar_Carro()
        return render(request, self.template, {'form': form})