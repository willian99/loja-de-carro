#coding: utf-8
import hashlib
import os

from django import forms
from loja_app.models.usuario import Usuario


class UsuarioForm(forms.ModelForm):

    first_name = forms.CharField(max_length=200,  widget=forms.TextInput(attrs={'class': 'form-control'}), help_text='Nome')
    cpf = forms.CharField(max_length=11,  widget=forms.TextInput(attrs={'class': 'form-control'}), help_text='CPF')
    username = forms.CharField(max_length=200,  widget=forms.TextInput(attrs={'class': 'form-control'}), help_text='Username')
    password = forms.CharField(max_length=32, help_text='Senha', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(max_length=100,widget=forms.TextInput(attrs={'placeholder': 'Informe seu E-mail'}))


    def save(self, commit=True):
        usuario = super(UsuarioForm, self).save(commit=False)
        usuario.set_password(self.cleaned_data['password'])
        if commit:
            usuario.save()
            return usuario

    def save(self, commit=True):
        usuario = super(UsuarioForm, self).save(commit=False)
        usuario.chaveAtivacaoUsuario = hashlib.sha1(os.urandom(128)).hexdigest()
        usuario.set_password(self.cleaned_data.get('password'))
        usuario.is_active = True
        usuario.is_staff = True
        usuario.save()
        usuario.save()
        return usuario




    class Meta:
        model = Usuario
        fields = 'first_name',  'email', 'cpf',  'username', 'password',
