from django import forms

from django import forms

from loja_app.models import Carros


class Form_Cadastar_Carro(forms.ModelForm): # uma funcao com o nome PostForm

    class Meta:
        model = Carros
        fields = ('modelo','cor','ano','preco','descricao','picture','marca')