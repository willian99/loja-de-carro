from django.db import models


class Carros(models.Model):

    modelo = models.CharField(max_length=200)
    cor =  models.CharField(max_length=200)
    ano =   models.IntegerField(max_length=200)
    preco  =  models.IntegerField(max_length=200)
    descricao =  models.CharField(max_length=200)
    picture = models.ImageField(upload_to="upload/imagem", blank=True)
    ativo= models.BooleanField(default=True)
    marca = models.CharField(max_length=100)

    def __str__(self):
        return self.modelo