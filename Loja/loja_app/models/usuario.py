#coding: utf-8
from django.db import models
from django.contrib.auth.models import User

class Usuario(User):

    '''
    cpf = models.CharField(max_length=11)
    rg = models.CharField(max_length=7)
    orgao_emissor_rg = models.CharField(max_length=6)
    uf_rg = models.CharField(max_length=2)
    data_emissao_rg = models.DateField()
    certidao = models.CharField(max_length=15)
    livro_certidao = models.CharField(max_length=10)
    folha_certidao = models.CharField(max_length=10)
    cartorio_certidao = models.CharField(max_length=10)
    data_nascimento = models.DateField()
    naturalidade = models.CharField(max_length=100)
    uf = models.CharField(max_length=2)
    nacionalidade = models.CharField(max_length=100)
    estado_civil = models.CharField(max_length=100)
    numero_filhos = models.CharField(max_length=2)
    titulo_eleitor = models.CharField(max_length=15)
    zona_titulo = models.CharField(max_length=10)
    secao_titulo = models.CharField(max_length=10)
    certificado_alistamento_militar = models.CharField(max_length=20)
    carteira_reservista = models.CharField(max_length=10)
    profissao = models.CharField(max_length=100)
    local_trabalho = models.CharField(max_length=100)
    genero = models.CharField(max_length=10)
    endereco = models.CharField(max_length=500)
    numero_endereco = models.CharField(max_length=5)
    complemento = models.CharField(max_length=200)
    bairro = models.CharField(max_length=100)
    cidade = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)
    cep = models.CharField(max_length=15)
    telefone_residencial = models.CharField(max_length=10)
    celular = models.CharField(max_length=10)
    telefone_comercial = models.CharField(max_length=10)
    curso_anterior = models.CharField(max_length=200)
    instituicao_curso = models.CharField(max_length=200)
    cidade_curso = models.CharField(max_length=100)
    uf_curso = models.CharField(max_length=2)
    ano_curso = models.CharField(max_length=4)
    rede = models.CharField(max_length=10)
    etnia = models.CharField(max_length=15)
    area_procedencia = models.CharField(max_length=10)
    autorizacao = models.BooleanField()
    fator_rh = models.CharField(max_length=3)
    problemas_saude = models.CharField(max_length=200)
    necessidades_especificas = models.CharField(max_length=100)
    transtorno_global_desenvolvimento = models.CharField(max_length=100)
    alta_habilidade= models.CharField(max_length=5)
    info_curso = models.CharField(max_length=100)
    carteira_trabalho = models.CharField(max_length=3)
    renda_familiar = models.CharField(max_length=50)
    faixa_etaria = models.CharField(max_length=50)
'''

    cpf = models.CharField(max_length=11,unique=True)
    chaveAtivacaoUsuario = models.CharField(max_length=255)


    def __unicode__(self):
        return self.username




