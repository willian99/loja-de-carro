
# -*- coding: utf-8 -*-
"""Teste para os formulários dos clientes"""
# python manage.py test
import unittest


class ClientTestCase(unittest.TestCase):
   def setUp(self):
       from loja_app.forms.usuario import UsuarioForm
       cliente1 = {'username': 'Will','password': '1234', 'email':'willian@gmail.com'}
       self.form1 = UsuarioForm(data=cliente1)

       cliente2 = {'username': 'Will', 'password': '1234', 'email':''}
       self.form2 = UsuarioForm(data=cliente2)

       cliente3 = {'username': 'Will', 'password': '', 'email':'willian@gmail.com'}
       self.form3 = UsuarioForm(data=cliente3)

       cliente4 = { 'username': 'Will','password': '1234', 'email':'willian@gmail.com'}
       self.form4 = UsuarioForm(data=cliente4)

       cliente5 = {'username': 'Will', 'password': '1234', 'email':'willian'}
       self.form5 = UsuarioForm(data=cliente5)

       cliente6 = { 'username': 'Will', 'password': '1234', 'email':'willian@gmail.com'}
       self.form6 = UsuarioForm(data=cliente6)

       cliente7 = { 'username': 'Will','password': '1234', 'email':''}
       self.form7 = UsuarioForm(data=cliente7)

   def test(self):
       self.assertEquals(self.form1.is_valid(), True)  # Todos os dados
       self.assertEquals(self.form2.is_valid(), False)  # CPF com letra
       self.assertEquals(self.form3.is_valid(), False)  # Sem senha
       self.assertEquals(self.form4.is_valid(), False)  # Sem nome
       self.assertEquals(self.form5.is_valid(), False)  # Sem @.com
       self.assertEquals(self.form6.is_valid(), False)  # Sem CPF
       self.assertEquals(self.form7.is_valid(), False)  # sem email
