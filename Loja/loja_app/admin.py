#coding: utf-8
from django.contrib import admin

from loja_app.models import Carros
from loja_app.models import Usuario

admin.site.register(Carros)
admin.site.register(Usuario)