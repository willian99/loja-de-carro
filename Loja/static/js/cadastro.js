$(document).ready(function () {

    // ===== MÁSCARAS ===== //
    $('.cpf input').mask('000.000.000-00', {reverse: true});
    $('.cnpj input').mask('00.000.000/0000-00', {reverse: true});
    $('.hora input').mask('00:00');
    $('.data input').mask('00/00/0000');
    $('.dois_digitos input').mask('00');
    $('.cep input').mask('00000-000');
    $('.rg input').mask('000000000');
    $('.dez_digitos input').mask('0000000000');
    $('.fone_ddd input').mask('(00) 00000-0000');
    $('.crm input').mask('00000000000000000000000000000000');
    $('.valor input').mask('00000000000000000,00', {reverse: true});
    $('.somente_numeros input').mask('00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
    $('.somente_letras input').mask('SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS', {
        'translation': {
            S: {pattern: /[A-Za-zÀ-ú ]/},
        }
    });


 });

